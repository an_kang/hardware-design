# 1. OPA记录

## 1.1. 参考资料
- [看懂这个，就真的懂TIA的稳定性了](https://training.ti.com/high-speed-transimpedence-amplifier-design-flow)
- [上面视频的中文翻译](https://e2echina.ti.com/blogs_/b/analogwire/archive/2020/09/04/6d7c2529_2d00_e2c7_2d00_43e5_2d00_bb25_2d00_e9b284da3031?tisearch=e2e-sitesearch&keymatch=opa858)
- [提取运放的输入电容](https://e2e.ti.com/blogs_/b/analogwire/archive/2016/03/21/spice-it-up-extracting-the-input-capacitance-of-an-op-amp-part-3)
- [分析TIA的稳定性：非常好](https://www.laserfair.com/peitao/201111/19/61630.html)
- [环路稳定性的基础，非常好](https://www.analog.com/en/technical-articles/loop-gain-and-its-effect-on-analog-control-systems.html)
- [别人的理解，环路稳定性](http://www.mythbird.com/guan-yu-ti-tinafang-zhen-yun-suan-wen-ding-xing-fen-xi/)
- [最学术的解释TIA](https://www.ti.com/lit/an/sboa122/sboa122.pdf)




## 1.2. 名词解释


## 1.3. 总结
- 要用环路增益去评估系统的稳定性，不是用的开环增益或者闭环增益！！！
	- 为什么？因为要在数学防止Aol*ß=1，同时避免ß=180°的时候Aol>1.这样系统就不会变成正反馈了！
- 要用闭环响应区评估系统对于时域的反应！！！
	- 因为系统是针对闭环来响应的，对输入信号。
	- 闭环响应看f-3db：这个就是我们设计系统的带宽，决定了时域响应的快慢。
- 设计TIA的时候，有两个公式。
	- 第一个公式：计算系统的闭环带宽f-3db的。我们的系统一般都对闭环带宽有要求，所以可以反推出GBP，在Rf和Ctot已知的情景下。
		- 为什么是这个公式：是在噪声增益的第一个零点和运放的GBP之间，求一个几何平均。
	- 第二个公式：计算Cf的值，让系统的闭环响应是巴特沃斯类型的，也就是Q=0.707。
		- 为什么默认了巴特沃斯。因为运放的Aol带一个极点，1/ß带的一个零点，Aol*ß就是2个极点，是个二阶系统。
		- 采用了f-3db当做fc来计算，意味着1/ß的第一个极点，就在和Aol交点的位置，那么就刚好可以补偿到45°的相位裕度。加上OPA本身有的裕度，就很客观了
	- ![Screenshot-2020-12-03 PM8](https://gitee.com/AndrewChu/markdown/raw/master/1609897171_20201203201644801_2058750464.jpg)
- 1/ß波形的理解：
	- 第一个零点：fz。主要是Ctot和Rf决定
	- 第一个极点：fp。是Cf和Rf决定的。
	- 最后收敛于：1+Ctot/Cf.当f足够大，就可以把R省略了，用s参数分析一下。
	- 怎么理解：直接把ß网络，也就是反馈网络拎出来做传函分析就知道了。也可以按照低频电容短路，中频Ctot起作用，最后Cf起作用来理解
	- ![Screenshot-2020-12-03 PM8](https://gitee.com/AndrewChu/markdown/raw/master/1609897167_20201203201222048_978077400.jpg)
- Cf的选值：
	- 会影响的因素：闭环带宽和过冲之间的balance
	- Cf的标准值，是按照巴特沃斯Q=0.707来选值
	- 如果Cf很小，那么1/ß的第一个极点的f会很高，运放的闭环响应就会以-40db/dec下降，总之就是稳定性不够，闭环响应会有高Q峰值，时域波形就会有震荡
	- 如果Cf很大，那么1/ß的第一个极点的f会很小，Aol*ß反馈深度会小，所以系统的闭环响应的带宽就会变低，那么就会导致时域波形变慢，可以按照Tr=0.35/fc来计算，因为时序的上升沿是由带宽内最高的f决定的
- 理解Q：
	- Q是一个二阶系统才有的概念。一阶系统是没有品质因素的。
	- 一阶系统fc=1/2pi*R*C，这个就是-3db点。原因就是实部和虚部相等
	- 二阶系统，fc是在实部为0，Q就是这个时候虚部值的导数。
		- 当为二阶巴特波斯类型的时候，fc=-3db点，和一阶系统对应了。所以大家都喜欢巴特沃斯。
		- 不为巴特沃斯，那么就要去看相移为-90度的点了。
		- 当Q>1的时候，那么就会有凸峰，意味着时域波形会震荡，会有稳定性的问题。
		- **理解K和Q的关系**
		- **理解f0和fc的关系**
			- f0：特征频率，使得二阶系统的分母实部为0
			- fc：cutoff转折频率，使得系统bode为-3db
			- 只有当巴特沃斯的时候，Q=0.707，K=fc/f0=1，fc=f0。其他响应只能知道fc和f0的大小关系
	- ![](https://gitee.com/AndrewChu/markdown/raw/master/1598600818_20200813200222205_1550229933.png )
- 要理解ß是什么？是反馈系数，不是增益系数
	- 画图的时候用的都是1/ß
- ![Screenshot-2020-12-02 AM10](https://gitee.com/AndrewChu/markdown/raw/master/1606903736_20201202180828648_960927141.jpg)
- ![Screenshot-2020-12-02 AM10](https://gitee.com/AndrewChu/markdown/raw/master/1606903739_20201202180835774_2122729505.jpg)
- ![Screenshot-2020-12-02 AM11](https://gitee.com/AndrewChu/markdown/raw/master/1606903740_20201202180841583_1086730241.jpg)
- ![Screenshot-2020-12-02 AM11](https://gitee.com/AndrewChu/markdown/raw/master/1606903740_20201202180848098_858675282.jpg)
- 理解1/ß曲线和Aol的交点
	- 下图包含了所有的零极点和f0
	- ![](https://gitee.com/AndrewChu/markdown/raw/master/1609897172_20210106093921406_1041234790.png)

# 2. OPA

# 3. 环路稳定性
In operational amplifier (op amp) applications, the feedback resistance of the amplifier interacts with its input capacitance to create a zero in the noise-gain response of the amplifier. This zero in the response, unless properly compensated, reduces the amplifier’s phase margin, causing a peaked frequency response with possible instability – or at the very least ringing in the pulse response.

Op-amp macro-models with inaccurate input capacitances will result to erroneous simulation results.


The differential input capacitance has a minor effect on loop gain, since the amplifier’s closed-loop, virtual-ground behavior bootstraps the input terminals together, thereby lowering the effect of the differential capacitance. The printed circuit board (PCB) parasitic capacitance on the inverting input pin will also affect the loop-gain response and hence its stability. You must take care during layout to minimize this parasitic capacitance.


## 3.1. 环路增益
ß是回馈系数：ß=R1/Rf
放大系数，也是闭环响应系数：1/ß=Rf/R1
Aol*ß：So the gap between the open loop gain curve and the closed loop gain curve is βA0。we can see that it is the loop gain (βA0) and its phase that determine the stability of the system.

The equations above show that the error is a function of how large βA0 is in relation to the “1” term in the denominator. Note that open loop gain alone does not always determine the error, but it is the product of the open loop gain (A0) and the feedback fraction (β) that is important. For large βA0, the “1” term loses significance; for βA0 near unity, the “1” becomes significant, increasing the error.


As a rule of thumb, for a single order low pass filter, at a tenth of the break frequency the phase shift is roughly zero. At each break frequency, the phase shifts by –45° (a phase lag) and at ten times the break frequency, the phase shift is approximately –90°, remaining there beyond. If the second break frequency is at 1MHz, then at 100kHz, the total phase shift of the filter is approximately –90°, at 1MHz the total phase shift is –135° and at 10MHz the total phase shift is approximately –180°.

由于Aol为运放固有特性，所以其一直不变，只需要观察1/β曲线即可。对于复杂的电路来说，反馈环路复杂，零极点很多。需要在Aol与1/β交点（Aolβ=0dB，fcl处，闭环穿越频率点）前将1/β拉直。


## 3.2. 总结
用这种方法仿真：
开环增益：Aol没有反馈电阻，跟随器的样式，因为b为1
环路增益：Aol*b，就是Vout。注意，要把环路断开，插入电感和电容
。用这个开判断相位稳定度和幅频稳定度。因为要防止1+Aol*b=0.
闭环增益：低频就是1/b，高频就是Aol


# 4. TIA设计要点
- ![Screenshot-2020-12-03 PM6](https://gitee.com/AndrewChu/markdown/raw/master/1606997105_20201203200358060_472459243.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997108_20201203200418874_1140392193.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997109_20201203200427231_902942819.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997110_20201203200436971_1447804634.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997110_20201203200442519_2038031040.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997111_20201203200448158_1337192220.jpg)
- ![Screenshot-2020-12-03 PM7](https://gitee.com/AndrewChu/markdown/raw/master/1606997111_20201203200458109_493474034.jpg)
