# 1. readme
1. `Rename_jpeg.py`
	1. 解释：从Photos拖下来的图片是jpeg的，但是Gitee只认jpg的。
	2. 用途：这个script可以把桌面的IMG开头的文件，重命名为jpg
2. `Rename_jpeg.py`
	1. 解释：因为手机拍的照片，都会很大，像素很高，就算是jpeg格式的也很大。但是如果图片大于1MB，在使用上会很麻烦
	2. 用途：这个script可以把识别所有桌面IMG开头的文件，然后读取像素，最后把像素限制在w和h都在2000以内
3. `cornerFreqenceCalculator.py`
	1. 解释：计算RC的截止频率
	2. 用途：输入R的值和单位，再输入C的值和单位

