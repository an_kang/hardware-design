# 1. 金句

-   Work commitments and family commitments
-   Attributing an idea to the wrong person or not giving credit to someone where it is due.
-   This film taught me how importance education is, and how education could change one's life. This film has also changed my perception of what defines a good job. Apart from the income, there are many other things I should consider, for example, whether the job is meaningful or not
-   But as long as they study hard, they can find work and get rid of poverty eventually
-   When I contacted the shop owner, he didn't manage to solve the problem and he also refuse to offer a full refund 
-   The service is terrible, and the shop owner didn't respect customer's right
-   They trend to manipulate potential buyers. Secondly, the product they sell are possible not of good quality. The standard of service is also poor.
-   Memorize some key concepts and theories 
-   To be honest, I don't care what I drink or eat right there, coffee tea or soft drink, what really matters most is that I can concentrate on my books there and read through notes.
-   But after all, it would possibly push me to move out of the comfort zone and improve my skills
-   Apply for a admission to postgraduate course in foreign country 
-   Many advertisements have focused on promoting their services by stressing their strengths, but this advertisement focus on out needs and interests.
-   I suppose that we can benefit a lot from the life experience of elderly people.
-   Find my place in the society. Self-awareness. Cognitive bias.
-   Goverment should encourage inhabitants to move to suburb or rural areas 
-   The policies are effective in land-scarce cities, which cannot meet the growing demand for housing
-   The restoration of old buildings in major cities in the world costs enormous government expenditure.
-   Money directly impact on our living standards. With money, we can live comfortable, and then experience happiness. Without it, you would possibly feel stressed out. If you are very wealth, but you do not have the company of  family, you are likely to feel lonely.
-   For instant, uniformed staff may learn how to communicate with clients in a friendly manner. 
-   In contrast, people who dress casually may not pay attention t o social rules and business etiquette, and other people may feel uncomfortable 
-   The economic downturn is another reason why many people cannot combine work and family commitments. They have to work long hours to impress employers. But overworking has made it difficult for them to balance work and family. This is a severe strain on working people.
-   Even some gentle physical exercise such as going for a jog, taking a brisk walk and swimming can burn calories, build our strength and help us keep a good body shape, increase blood circulation
-   Playing sports can improve our physical health and reduce sick leave. We are more productive and less prone to health problems
-   They may eat fatty food, which is harmful to their health. Fast food such as fried chicken, fish and chips, fizzy drinks contain salt and sugar which can cause health problems including heart diseases and obesity.
-   I enjoy work along without be supervised or interrupted by others
-   in contrast, children exposed to domestic violence will also behave badly at school and show aggression 
-   the labour market has become intensely competitive, and the unemployment rate has been rising constantly
-   You should make sure that your products can satisfy the needs of customers and possibly serve a niche market. Your porducts can thus stand out from crowd. You also need to have adequate capital to set up a business
-   Participants will feel that if they experience difficult times in the future, they're likely to gain support from other members of community. They'll feel part of community.
-   I suppose that people play sport in order to keep fit and lead a healthy life. But sport can be a social activity as well
-   Subsidies for pesticides and artificial fertilizers encourage farmers to use greater quantities than are needed to get the highest economic crop yield 
-   The capacity of environment to sustain economic development and population is finite 
-   The percentage of women students at the university has increased steadily
-   I am looking for a job, but have not found my ideal yet
-   The idealism of the younger generation, or youngster 
-   Pupils' responses indicate some misconceptions in basic scientific knowledge of rainforests' ecosystems
-   He is not light-hearted as he used to be -- too much responsibility 
-   Let us consider the pluses and minuses of changing the system.
-   Their performance was inferior to that of other team 
-   please explain why you think there is a problem then suggest a reasonable solution. 
-   even moderate amount of this drug can  be fatal
-    Property prices have shot up rapidly 
-   The convention presents fundamental strategies and approaches in achieving health 
-   It was then that I conceive the notion of running away
-   Do not interfere in what do not concern you
-   Good health is  a major resource of society, economic, and personal development and an important dimension of quality of life
-   This article initiated a debate on the task of medicine, its professional obligations, social position and moral justification
-   ‘Do not put all your eggs in one basket’ is a proverb
-   Stress has been linked with many so-called modern disease such as heart disease, strokes and even cancer 
-   Psychologists have conducted researches showing that people become less sceptical and more optimistic when the weather is sunny
-   Employees will perceive that rewards or outcomes are equitable and equal to the inputs given 
-   The building makes a tremendous visual impact 
-   This phenomenon has become increasingly apparent in modern industrialized societies.
-   his greatest character trait is his honesty 
-   Broaden one's horizon 
-   a flush of enthusiasm 



# 词汇

## 常用句子

| I am not quite sure how to express this exactly in English in short sentences |
| ------------------------------------------------------------ |
| This question is a little bit hard even to express in Chinese. I will try to  figure out how to explane it in brief words. |
| Because it is a long time ago, I read this book. Or the story is comprehensive. Or the theory is abstract. Or the culture in China is slightly different with western country. Something maybe reasonable, will be ridiculous in western country. |
| I can show you an example which maybe not very properly, but it can stand for my personal concept. |
| Could you rephrase the question                              |
| Did you mean that why ...                                    |
| Take different factors into consideration                    |
| Have opportunity to advance one's career                     |
| Produce high quality film to compete with foreign movies     |
| in society, you have to obey rules and comply with laws      |
| I presume that, make presumption that， suppose              |
| Since English is not my mother tongue, so  I need some pause to figure out the properly and correctly usage of some certain vocabulary |
| what she feels for him is akin to worship                    |
| I spent three weeks reading and doing general research and then I dashed the writing off very quickly |
| Teenagers start smoking usually due to peer mutual effect    |
| Listening to lectures in many ways is just giving you information that you could access for yourself in library |
| Most insects are harmless and play an essential role in maintaining the balance of nature |
| A still picture could only imply the existence of time, while time in novel passed at the whim of the reader. But in cinema ,the real, objective flow of time was captured |
| The dawn of civilization / time                              |
| Although both  programs had similar effects on productivity, they has significantly difference results in other respects. |
| Garbage collection/disposal                                  |
| Cross nation comparison can also shed light on this question |
| A variety of organizations have evolved over time  to accommodate the needs of company and society |
| the results of this survey can be divide into three main categories |
| We must proceed with extremely caution. I would caution against getting too involved. |
| I have no illusions about my ability                         |
| I would like to illustrate with reference to a specific event which occurred several years ago |
| Social tensions are manifested in the recent political crisis |
| The history of human civilization is entwined with the history of the ways which we have learned to manipulate the water resources |
| You will need to persevere if you want the business to succeed. |
| I struggled to retain control of this situation              |
| Is watching TV a retreat of reality?                         |
| employees have to work 13-hours a day, from 9 am to 10 pm, this is an ordinary routine in some department. There is no time left for leisure and family commitments |
| I am going to give you a brief account of the history of this museum before let you roam about on your own |
| The question is one that can never be answered by any degree of certainty i |
| bookstores now abound with manuals describing how to manage time and cope with stress |
| We have to look at everything from international perspective |
| Many people in our era are drawn to the pessimistic view that the new media are destroying the old skills |
| you need to compare the tabloid newspapers with those of 50 years ago |
| Hypotheses are imaginative and inspirational in character    |
| I would like to say a sincere thank you to everyone who has helped and supported me |
| Until several years ago, there was still a taboo around the subject of divorce |
| detectives deduce from the clues who had committed the crime |
| These systems were implemented with varying degrees of success and criticism |
| My philosophy is , l leave work at 5 o'clock and forget all about it till the next day |
| One of the pillars of a civilized society must be that everyone has equal access to legal system |
| The job is tough, but the financial rewards are worthy       |
| standards which are imposed vary greatly from country to country and industry to industry |
| Children  may take the heroes of TV programs as a role model and copy their behaviour |
| Doing the household/domestic chores                          |
| could you tell us something about your first impressions of the town when you arrived? |
| Exceptionally great mental or creative ability               |
| I was an enthusiastic student and I never found it difficult to find the incentive to paint |
| the sense of ownership exists in may forms: the home, the street, the village, town, city, province, and eventually country |
| He was a romantic at heart and longed for adventure          |
| Students are handing out election leaflets at the station    |
| Human welfare is the ultimate goal of economic activity      |
| People were demanding that their fundamental right to healthcare be satisfied by state |
| remember to visit the souvenir stalls in the car park in front of the main entrance to the stadium |
| code of practice                                             |
| We underestimated the time it would take to get there        |
| For a relatively new industry it is not surprising that the ecotourism has undergone teething pain |
| No one is denying that books are competing with other forms of entertainment for children's attention |
| This is surprising considering the high level of media coverage on this issue |

 t

## 连接词

|                                   |                        |                                   |
| --------------------------------- | ---------------------- | --------------------------------- |
| apart from this                   | as long as             | Consequence of                    |
| Eventually                        | Definitly              | Fantastic                         |
| But after all                     | literally in English   | Choice and consequence            |
| quarrel argument and fight, blame | fairly                 | continuously                      |
| Well-rounded education            | if necessary           | conscious of ... responsibilities |
| Show determination to win         | Moral principles       | Develop qualities                 |
| A good influence                  | Encounter difficulties | prerequisite                      |
| As soon as                        |                        |                                   |

## Characters

| Positive                                                     | Negative           | Neural |
| ------------------------------------------------------------ | ------------------ | ------ |
| compassionate, aggressive, ambitious, Improve self-esteem, perseverance | get distracted     |        |
| Advantages, avoid procrastination, cope with stress, put a severe strain on ... | Disadvantages      |        |
| Polite, sociable person, repay home loans                    | Fell stressed      |        |
| Efficient, patient                                           | absent-mind        |        |
| Follow friends' advice as to how to find good company        | Run out of time    |        |
| Grasp this opportunity, you can earn a fortune easily, possibly as the manner of online business success | Good grades        |        |
| Conducive to learning                                        | procrastinate      |        |
| Balance Theory and practice                                  | pathetic           |        |
| Stressful, selfish                                           | partial            |        |
| Tend to be  aggressive , driven by self-interest, motivated by greedy, commit crimes | vulgar，vulnerable |        |
| Conscious of other people's interest, show compassion for others, responsive to other's emotional needs. |                    |        |
| zealous， authentic，convincing，endorse，precursor，preliminary，prestigious，susceptible |                    |        |



## Daily Life

| Positive                              | Negative                                 | Neural |
| ------------------------------------- | ---------------------------------------- | ------ |
| Help needy people                     | Experience difficulties in my life       |        |
| For free                              | Children from a less well-off backgrouds |        |
| Festival, festive season              | A densely populated city                 |        |
| Browsing website when I am at home    | Racial discrimination                    |        |
| Construction of residential buildings | Leading a  hectic life                   |        |
| Tasks at hand. Reduce anxiety         | Live an inactive life                    |        |
| Live coverage of video game           |                                          |        |
| Go for a jog, reduce stress           |                                          |        |
| Launch awareness campaigns            |                                          |        |

## Family

| Positive                                            | Negative | Neural |
| --------------------------------------------------- | -------- | ------ |
| Domestic works, domestic responsibility             |          |        |
| Balance work and family commitments                 |          |        |
| Basic necessities                                   |          |        |
| Material possessions, live in a materialistic world |          |        |
| Up-to-date equipment, the most advanced model of    |          |        |

## Friends

| Positive                                                     | Negative  | Neural |
| ------------------------------------------------------------ | --------- | ------ |
| Forge a strong relationship                                  | Detriment |        |
| Circle of friends, build a network of contacts               | Get upset |        |
| Hang out with friends, peer pressure, socialise with neighboure | Feel bad  |        |

## Jobs

| Positive                                                     | Negative              | Neural |
| ------------------------------------------------------------ | --------------------- | ------ |
| A sense of achievement, rach a crossroad of my career path, master a foreign language | Gender discrimination |        |
| Employment opportunities                                     |                       |        |
| Want Autonomy, show discipline, work from home, create now possibilities, enter the workforce |                       |        |
| Multinational enterprise , international company, renowned institution, improve job prospects | Gerder bias           |        |
| Serve as a good role model, prestigious company              | Glass ceiling         |        |
| A team conducted a survey about somethings                   | Gender equality       |        |
| We are able to understand the challenges of children from less developed area are facing |                       |        |
| Solve problems related to something                          |                       |        |
| Good at different subjects                                   |                       |        |
| expand your horizon, follow advice, increase knowledge       |                       |        |
| Decent job, Well-paid job, earn profits, earn fortune        |                       |        |
| What qualities the job requires                              |                       |        |
| Run a business, work from home                               |                       |        |
| Online shop, e-commerce , has gone mainstream, seize this business opportunity, place an order, deliver products to customer on time, offer discount, match your need easily |                       |        |
| Tight timetable, tight schedule, give priority to            |                       |        |
| Improve language skills, have a good command of English      |                       |        |
| Survive in another country or company                        |                       |        |
| Speak fluent English, adapt to local culture                 |                       |        |
| Members of the board                                         |                       |        |
| on the spot, in the field , commutor                         |                       |        |
| first language， mother tongue, culture identity ,dominant language |                       |        |
| Hand-on experience , careers I pursue in the future          |                       |        |
| Complete degree course, take vocational course               |                       |        |
| collaborative teamwork, embark on a team project             |                       |        |
| Apprciate the effort of every participant, impose their opinions on others |                       |        |
| devote too much time to work                                 |                       |        |
| combine work with pleasure                                   |                       |        |
| Extra curricular activities                                  |                       |        |



## Environment 

| Positive                                                     | Negative | Neural |
| ------------------------------------------------------------ | -------- | ------ |
| Prosperous                                                   |          |        |
| Conducive to learning                                        |          |        |
| Relaxed atmosphere                                           |          |        |
| Ordinary from outside                                        |          |        |
| Revise for exam                                              |          |        |
| All visitors are expected to keep their voice down when talking to each other |          |        |



## Education

| Positive                                                     | Negative                   | Neural                 |
| ------------------------------------------------------------ | -------------------------- | ---------------------- |
| Get rid of poverty                                           | Commit crimes              | Acdemic work, homework |
| Recruit teacher in rural areas                               | Deprived area 缺少教育地区 |                        |
| On a one-on-one basis                                        |                            |                        |
| those in need                                                |                            |                        |
| Provide tutorials                                            |                            |                        |
| Prestigious university, graduate from university, further education |                            |                        |
| Broaden our mind                                             |                            |                        |
| Pubic services                                               |                            |                        |



## Politics

| Positive           | Negative                | Neural |
| ------------------ | ----------------------- | ------ |
| A deterrent effect | Surveillance cameras    |        |
| Combat crime       | Pay a fine              |        |
| Work as volunteer  | Vocational job training |        |
|                    | Escape punishment       |        |



## Habits

### leisure Time

| Positive         | Negative | Neural                             |
| ---------------- | -------- | ---------------------------------- |
| Leisure activity |          | Watch video and check blogs online |
|                  |          |                                    |
|                  |          |                                    |
|                  |          |                                    |



### Book

| Positive | Negative | Neural |
| -------- | -------- | ------ |
|          |          |        |
|          |          |        |
|          |          |        |
|          |          |        |



### Film

| Positive | Negative | Neural |
| -------- | -------- | ------ |
|          |          |        |
|          |          |        |
|          |          |        |
|          |          |        |



### exercise

| Positive | Negative | Neural |
| -------- | -------- | ------ |
|          |          |        |
|          |          |        |
|          |          |        |
|          |          |        |



### Celebrity

| Positive            | Negative | Neural |
| ------------------- | -------- | ------ |
| Famous entrepreneur |          |        |
|                     |          |        |
|                     |          |        |
|                     |          |        |























































