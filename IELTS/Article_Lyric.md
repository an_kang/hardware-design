# 1. Article_Lyric

这个文章的目的是记录好文章，给写作和口语作为素材。

# Article

## 异地恋如何维护感情

>   Most of us dream of meeting that special someone. For lack of a better phrase, that prince and princess charming who will sweep off our feet.
>
>   But to quote the old adage from shakespeare, 'the course of true love never did run smooth'
>
>   Sometimes our dream partner may **have an unfortunate surprise to share with us**. They may not live in the same town, city, country, or even on the same continent as you.
>
>   And so some crazy lovebirds decide to enter into a long distance relationship
>
>   **We live in a time when the internet facilitates easier interaction with your cherished and treasured potential life partner** 
>
>   However, according to the many people who have been in low distance relationships, **there are certain pitfalls that you need to be aware of to enjoy a committed and monogamous relationship over the wire of internet** 
>
>   **Many people are put off by the very idea of it. A lack of intimacy and companionship can act as a deterrent for some**. But for those who embrace a digital distance relationship, they must accept that at times they may feel lonely.
>
>   Many of us dream of that inseparable bond, often named as soulmates, and here are three tips to help you navigate this potentially tricky form of dating.
>
>   Firstly, there needs to be trust. **Being faithful is a prerequisite in most relationships, but you also need to be able to trust yourself not to stray**. Part of the challenge is dealing with loneliness. **Also, maintaining a leve of self-sufficiency and setting goals is another way to help you cope.**
>
>   **Not only does it mean you keep your independence, but also gives you more to talk about with your partner** 
>
>   Lastly, you need a plan, dates when you will see each other again, or where the relationship will lead to: perhaps even marriage.
>
>   So, long-distance relationships are potentially a difficult situation, but if you do meet Mr. or Mrs. Right, and they live far away, **there are some steps you can take to make your life a litter easier.**



# Lyric

## I Am Moana

>   I know a girl from an island
>
>   She stands apart from the crowd 
>
>   She loves the sea and her people
>
>   She makes her whole family proud 
>
>   Sometimes the world seems against you
>
>   The journey may leave a scar
>
>   But scars can heal and reveal just where you are
>
>   The people you love will change you
>
>   The thing you have learned will guide you
>
>   And nothing on earth can silence the quiet voice still inside you
>
>   And when that voice starts to whisper 
>
>   Moana you've come so far
>
>   Moana listen, Do you know who you are 
>
>   
>
>   Who am I?
>
>   I am a girl who loves my island 
>
>   I am the girl who loves the sea
>
>   It calls me
>
>   I am the daughter of village chief
>
>   We are descended from voyagers who found their way to across the world
>
>   They call me 
>
>   I've delivered us to where we are
>
>   I've journeyed farther
>
>   I am everything I've learned and more
>
>   Still it calls me 
>
>   And the call isn't out there at all
>
>   It's inside me
>
>   It's like the tide
>
>   Always falling and rising
>
>   I will carry you here in my heart
>
>   You'll remind me
>
>   That come what may
>
>   I know the way
>
>   I am Moana!
>
>   





























