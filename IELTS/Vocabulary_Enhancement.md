# 1. Vocabulary Enhancement



## 1.1. A
1. arch：拱门
	1. anchor：锚
2. arctic： 北极洲
	1. antarctica：南极洲
3. authentic：真正的
    1. pathetic 悲伤的 
4. ambiguous 模糊的
5. Anticipate 预料v
    1. participate 参与
6. Assort 归纳 分类v
    1. resort 处置武力v
7. adverse 不利的adj
    1. adversary 对手n
    2. adversity 不利的情况
    3. advertise 做广告v
8. Anticipate 预料v
    1. participate 参与v
9. acclaim 称赞
    1. declaim 抨击
    2. Claim 声称
    3. Declare 声称

## 1.2. B
- barrier 障碍
	- barrel 一桶
	- obstacle 障碍
- bulletin：公告
	- bullet：子弹
- beard：有胡子的
	- bead：珠子
- bold：大胆的，粗体的
	- bald：秃顶的，磨平的
- bumble：笨手笨脚，跌跌撞撞
	- stumble：跌倒，结巴
1. blaze：烈火
	1. blazer 夹克
	2. blazer：夹克
	2. blade：刀锋
1. board：董事会 甲板
	1. boarder 寄宿
	2. broad： 广阔的
	3. border 边界
	4. boundary 边界
1. burger 汉堡
    1. burglar 窃贼
1. boast 自吹
    1. blast 爆炸
1. blend 混合v
    1. Blent 混合v（过去分词）
    1. bend 弯曲v
    1. bent 弯的adj
    1. Belt 安全带





## 1.3. C
9. contain 包含 扼制
    1. contamination 污染物
    2. Curb 扼制
    3. crab 螃蟹
10. couch 长凳
    1. coach 教练
- circumference 周长
	- circumstance 环境
	- curriculum 全部课程
- crow 乌鸦，啼叫
	- cow 牛
	- crown 皇冠
	- crowd 群众
	- clown 小丑，乡下人 
- crust 壳
	- crusty 有硬皮
	- crush 挤压
	- crash 撞击
- cave：洞穴
	- carve：雕刻
	- crave：渴望
- complain：v抱怨
	- complaint n抱怨
	- compliant：符合
	- compile：编译
	- complement 补充
1. cot：简易床
	1. cotton：棉花
	2. cottage：小屋
	3. college：大学
	4. colleague：同事
2. commit：犯下，承认
    1. Committee:委员会
3. cosmic 宇宙的
    1. cosmetic 化妆的 门面的
4. custom：习俗
    1. Customer:客人
    2. costume：戏服
    3. consume：消费
5. crucial 至关重要
    1. cruel 残忍的
6. creep 爬行
    1. creepy 毛骨悚然
7. contract 合同
    1. contrast 对照
    2. contain 包含 扼制
8. criteria 判据
    1. Criticise 批评
9. coarse 粗糙的
    1. course 课程
    2. coast 海岸
10. conscience 良心
    1. conscious 意识到
11. Collaborate 合作
     1. elaborate 复杂的，精心制作的
 12. contrary 相反的adj
      1. contrast 对比v
      2. contradict v 矛盾
      3. contradictory 矛盾的adj
      4. contact 沟通
      5. contract 合同
      6. consistent 一致的
      7. constrain 扼制
 13. corporate 社团的
      1. incorporate 吸收v
 14. coach 教练 指导v
      1. couch 长凳子
 15. coarse 粗糙的
      1. coast 海岸
 16. Controversy 争论n
      1. conversation 对话n



## 1.4. D

4. debt 欠债
    1. doubt 无疑
1. deduct 扣减
	1. deduce 推导
	2. depict 描述v
	3. depress 使沮丧v
	4. disperse 分散v
	5. dissemble 掩饰  假装v
	6. dissent 散播
	7. dissipate 消散v
1. deceit 欺骗n   deceive 欺骗v
	1. receive 接收
1. depose：免职，作废
	1. deposit：保证金
	2. disposal：可以丢弃的
1. despite：不管
	1. despise：鄙视
1. drawback：缺点
	1. withdraw：撤回
1. domain：领域
	1. dominate：统御
	2. dominace：统治
1. daily：日常的
	1. diary：日记
	2. dairy：牛奶
12. detrimental：有害的
     1. diminish：减少
 13. dispute 争吵
 14. diversity 多样性
 15. despair 绝望
      1. desperate 不顾一切
 16. Deficiency 
 17. discrete 分立的
      1. district 地区
       2. Discretion 判断

## 1.5. E
1. endemic 地方的
	2. epidemic 流行病
	3. pandemic 世界流行病
5. excess 过多的
    1. exercise 锻炼
13. Evoke 激起
    1. evolve 进化
    2. revolve 旋转
    3. involve 包括
    4. revoke 取消
1. emerge：出现
	1. merge：合并
	2. emergency：紧急
	3. immense：巨大的
5. extrinsic 外界的
    1. intrinsic 内部的
    2. subtle 精细的
6. embassy 大使馆
    1. ambassador 大使
7. Engage:吸引，订婚
8. Enthusiastic : 热情的
9. Empirical 经验主义的
    1. empire 帝国
10. elaborate 精细的
    1. collaborate 合作v
11. elicit 得出v
     1. explicit 明确的 直言的


## 1.6. F
1. flare 燃烧
	1. flame 火焰
	2. blaze 烈火
	3. frame 一帧
	4. famed 有名的
	5. blade 刀锋
	6. spade 黑桃 铲子
12. Fraud 欺骗
    1. fraught 充满
3. fasten 扣紧
4. famine 饥荒
    1. feminine 女性的
5. Fancy 想象力
    1. fantasy 白日梦
    2. fantastic 极好的
6. Forbid 禁止
    1. prohibit 禁止
    2. ban 禁止
7. favour 喜欢
    1. favourite 最喜欢
    2. flavour 味道


## 1.7. G
1. gaze 凝视
	1. glance 瞥了一眼
	2. graze 放牧，吃草
1. gloom: 黑丝的
	1. groom: 整洁的
	2. glow：微光
	2. bloom: 开花
	3. broom:扫帚
1. grand 隆重的
    4. grant给予
1. genuine 真的，真诚的
    1. gene 基因
    1. Genus 类 属
    1. genetic 基因的
    1. genius 天才



## 1.8. H
1. hospital：医院
	1. hostile：敌视
	2. hostel：收容所
	3. hotel：旅店


## 1.9. I
1. lumber 原木
	1. timber 原木
	2. lump 肿块
	3. plumb 下水道
3. immense 巨大的
	1. immerse 沉浸
	2. immerge 沉浸
	3. merge 合并
	4. Emerge 出现
	5. Enumerate 列举
	6. Enormous 大量的
	7. Massive 大量的
7. intimate 亲密
   
    1. timid 胆怯的
1. irritate：激怒
	
	1. irrigate：灌溉
1. incubator：恒温箱
	1. cub：幼崽
	2. cab：计程车
6. infer 推断
    1. interfere 干涉
    2. intervene 干涉
    3. interpret 解释
7. indignant 愤怒
   
    1. indignity 轻视
8. Impose 迫使
    1. impart 给予
    2. impartial 公平的
    3. partial 部分的,偏袒的
    4. portion 部分
9. idiom 成语，方言
   
    1. idiot 傻瓜
10. immense 巨大的

    1. immerse 沉浸
2. imminent 逼近的
   
    3. eminent 杰出的
12. merge 合并
11. irrigate 灌溉

     1. irritate 激怒
12. infringe 侵犯

     1. fringe 边缘 附加的
     2. linger 逗留 徘徊
 15. Incidence 发生率n

      1. incident 小事n
 16. inflame 发炎

      1. inflation 通货膨胀

17.  indigent 贫穷的
     1. diligent 勤勉的
     2. indignant 愤怒的
     3. indigenous 土生土长的，生来的
     4. dignity 尊严

## 1.10. J
2. jar 罐子
	1. jam果酱
	2. jaw下巴
3. Journal 日记，杂志
    1. journalist 记者
    2. journey 旅程



## 1.11. K
1. knob：把手，旋钮


## 1.12. L
8. laundry 洗衣房
    1. lounge 候机室
1.  leather 皮革
  - leash 束缚，皮带
  - feather 羽毛
2.  lid：盖子，眼睑
  - lick：舔
  - lad：小伙子
  - ladder：梯子
1. lava 火山
	1. larva 幼虫
	2. lavatory 厕所
2. lose 丢失v lus
    1. loss 损失n los
3. lump 肿块
    1. lamp 羊羔
    2. lame 跛脚的
    3. limp 柔软的， 蹒跚
    4. limb 四肢
    5. lumber 木材
    6. linger 逗留 拖延
4. lend 出借v
    1. rent 出租v n
    2. rend 撕开，抢夺



## 1.13. M
1. mug 杯子
	1. mud 泥泞
1. moral 道德
	1. mortal 永生
	2. moan 呻吟
1. mutter：咕哝
	1. mutton：羊肉
1. metal 金属
    1. mental 精神
1. mute 哑巴
    1. mutual 互相的
1. Maximize v
    1. Maximum 最大值，最大的n adj
    1. Minimize 
    1. Minimum
1. mug 杯子， 傻瓜
    1. mud 污泥
    1. muggy 湿热的
    1. muddy 泥泞的
    1. bud 嫩芽
    1. dub 傻瓜
1. Morality 道德标准
    1. mortality 死亡人数

## 1.14. N
1. naught：零，无价值
	1. naughty：淘气
2. nominate 提名
    1. nominal 名义上的
    2. neutral 中性的
    3. neural 神经的
3. Nicety 准确 细节
    1. meticulous 谨慎的
    2. precision 精确的
    3. nice 好的
4. Notion 见解
    1. notoriety 声名狼藉的
5. Nourish 养育
    1. nutrition 营养


## 1.15. O


## 1.16. P
11. ponder 思考
    1. powder 粉末
- pitch 投球，投，定高音，颠簸，斜坡
- perk：振作，津贴
	- perch：栖息，高处坐着
- passage：段落
	- passenger：乘客
- psychiatrist:精神病学家
	- psychologist：心理学家
- plough：梨，北极星
	- laugh：大笑
- pivotal：重要的
- passage 文章
    - passenger 乘客
- pedestrian 行人
- parallel 并行
    - series 串行
- perspective 观点，透视
    - prospective 预期的，未来的
    - perception 认知能力
- profound 意义深远的
    - prolong 延长
    - prominent 著名的
    - prestigious 有威望的
    - protest 主张 抗议
    - proverb 谚语
    - provoke 激怒
    - prohibition 禁止
- posture 姿势
    - pasture 牧场
    - gesture 手势
- patent 专利
    - pattern 模式
- profit 利润
    - prolific 多产的
- Prison 监狱
    -  poison 有毒的
    - jail 监狱
- Persevere 坚持v
    - perserve 预留v
- precise 精确的
    - precious 宝贵的
- precede 先于
    - preceding 之前的
    - proceed 推进
    - progress 进步
    - process 流程
    - procedure 流程
- punctual 守时的
    - puncture 穿刺
    - penetrate 穿透
- plagiarism 剽窃
    - plague 瘟疫
- Premiere  首映 女主角
    - premium 保险的
    - prime 主要的
    - Primary 基础的
    - primitive 原始的

## 1.17. Q
- quilt	被子
	- quit 退出
	- quiet 安静
	- quiescent jin
- pump：泵
	- bump：撞击，肿块


## 1.18. R
1. rigid 严格的
	1. rigorous 谨慎
	2. vigorous 精力旺盛
3. relative 相对的
	1. relevant 有关的
	2. relate 联系
- repent 后悔
	- resent 怨恨
- reap 取得，收获庄家
	- rape 强奸
	- ripe 催熟
1. refuge：避难所
	1. referee：裁判人
1. range：范围，山脉，排列
	1. ranger：游骑兵，护林员
1. Rack：行李架 齿条
1. Reform: 改革
    1. revolution：革命
    1. evolution：净化
    1. revoke：撤回
    1. invoke：激起
1. retail 零售
    1. rental 租金
    1. resent 憎恨
    1. repent 后悔
    1. regret 后悔
1. Rage 愤怒
    1. range 范围
1. Religion 宗教
    1. region 地区
1. response：负责
    1. respond：回答
1. renowned 有名的
    1. renewal 更新
1. Receipt 收据
1. perch 边缘
     1. peck 啄
1. pear 梨树
     1. peel 削皮
     1. Peer  凝视， 同龄人
1. rigorous 严格的 谨慎的
      1. vigorous 精力旺盛的
1. resent 憎恨
      1. resemble 类似
1. Remarkable 可观的
      1. significant
      1. expressive
      1. considerable
1. relieve 减轻
      1. receive 收到
      1. receipt 收据
      1. recipe 食谱
      1. reception 前台
1. repeal 废除
      1. Appeal 呼吁

## 1.19. S
1. sketch 草图
	1. scratch 抓 挠
	2. scrape 刮掉
	3. scrub 擦掉
	4. stretch  拉伸
1. stiff 固执的
	1. stiffen 僵硬的
3. squirrel 松鼠
    1. quarrel 吵架
1. statute 法规
	1. statue 雕塑
	2. status 状态
1. stem 花茎
	1. stern 严厉的
5. swap 交换
	1. swamp 泥沼
1. saw 看见 锯子
	1. sow 播种
	2. sew 缝制
- slum 平民窟
	- slam 猛冲
	- slim 苗条
	- slump 暴跌
- suite：套房
	- sweet：
	- suit：适合
- scarce：缺乏，勉强
	- sacrifice：牺牲
	- scare：害怕
	- scar：伤疤
- sign：签名
	- sigh：叹气
1. soil：土壤
	1. soiled：污染的
	2. soybean：大豆
1. spring：春天
	1. sprint：冲刺
	2. spurt：冲刺，喷出
1. saw 锯子 看见
	1. sew 缝合
	2. sewer 下水道 阴沟
	3. sewerage 排水设备
1. subordinate：下属，次要的
1. straight 直接
    1. strait 海峡
1. stagger 蹒跚
    1. stagnant 停滞不前
1. susceptible 易受感染的
    1. suspend 推迟v
    1. suspicious 可疑的
    1. spectator 观众
    1. spectacles 眼镜
    1. spectacle 大场面
    1. spectacular 壮观的
    1. speculate 猜测的
    1. skeptical 怀疑的
1. systematic 系统的
    1. symmetry 对称n
1. sanitary 卫生的
    1. sanction 批准
1. shallow 浅的
    1. shadow 阴影
    1. swallow 吞
    1. swollen 肿的
1. recipes 菜谱
     1. therapy 治疗
1. terrain 地形
     1. territory 领地
1. Surveillancedd 监视
1. stationary 静止的
      1. stationery 文具
1. stain 污点
      1. strain 压力
1. strength 力量 加强
      1. stretch 拉伸
1. Salvage 拯救
      1. Savage 野性的
1. Carnivore 食肉动物 
      1. Carnival  嘉年华




## 1.20. T
1. timid 胆小的
	1. timidity 胆小 害羞
	2. intimidate 恫吓的
	3. intimate 亲密的
1. trend 趋势
	1. tread 踩踏
	2. thread 线程
	3. treadmill 枯燥的工作 跑步机
	4. tender 温柔，投标
	5. render 给予
	6. Gender 性别
1. tuition 学费 教学
	1. intuition 直觉
	2. institution 组织
1. throne 王座
	1. thorn 刺
6. trail 踪迹
	1. trial 审判
1. tense 拉紧 紧张，时态
	1. intense 强烈的
2. tutor：导师
	1. tutorial：教程，教学的
	2. institution：协会
	3. seminar：研讨会
1. transit：运输，穿过
	1. transient：瞬时的
	2. transmit：发射
	3. transition：过渡，转换
	4. transport：运输
	5. transistor：晶体管
	6. transceiver：收发器
1. tide 潮汐
	1. tidy 整洁
10. tribe：部落
    1. tribute：致辞
11. theft 盗窃n
    1. thieve 盗窃v
    2. thief 小偷
12. territory 领土
    1. terrible 可怕
13. thorough 彻底的
    1. through 通过
    2. thought 想法
14. trumpet 喇叭 鼓吹v
    1. triumph 胜利 成功
    2. trophy 战利品
15. trivial 小事
    1. rival 对手
    2. subtle 细微的 狡猾的
    3. tribute 致辞n
16. Tempt 诱惑v
    1. lure 诱惑
    2. attempt 企图
    3. Temptation 诱惑n
    4. temperate 气温温和，克制的
    5. temperature 温度
    6. contemplate
    7. Template
17. Tuition 学费 课时
    1. tutorial 辅导课
    2. tutor 导师



## 1.21. U

1.  universe 宇宙
    1.  university 大学
    2.  universal 宇宙的
2.  unanimous 全部同意的
    1.  anonymous 匿名的
    2.  autonomous 自治的
    3.  automation 自动化
    4.  automobile 汽车
    5.  automatic 自动化的
    6.  automotive 汽车的
    7.  

## 1.22. V
1. venue
	1. revenue：收入
	2. avenue：林荫道
1. vocation：职业
	1. vacation：假期
1. vehicle 车辆
    1. automobile 汽车
    1. public transport 公共交通
    1. congest 堵车
    1. traffic jam
1. visual 视觉的
    1. vital 至关重要的
    1. virtual 虚拟 实际的
    1. virtue 美德
    1. virus 病毒
1. vaccine 疫苗
    1. vacuum 真空
    2. Vacant 空的
    3. vacancy 空白n
    4. vacation 假期
1. violent 暴力的
    1. violet 紫罗兰
    1. violence 暴力
    1. volatile 可变的
    1. voluntary 自愿的
1. vulnerable 易受攻击的
    1. vulgar 庸俗的
1. wrinkle 皱纹
    1. twinkle 眨眼
1. vogue 时尚
    1. vague 含糊的
    1. voyage 航线


## 1.23. W
2. weed 野草
    1. weep 哭泣
    2. sweep 扫
    3. wipe 擦
    4. swipe 挥
    5. wheat 小麦
    6. cheat 欺骗
1. wreck 破坏 损坏
	1. rack 架子，折磨
	2. reck 思考
	3. rake 耙子
	4. wreckage 残骸
	5. ragged 波波懒懒的
	6. Wretch 不幸的人
	7. wretched 无法容忍的
1. waise：腰部
	1. waste：浪费
	2. wrist 手腕
	3. wrestle 摔跤v
5. wander 散步
    1. wonder 好奇
6. worship 崇拜
    1. warship 战舰
    2. adore 爱慕


## 1.24. X



## 1.25. Y



## 1.26. Z













