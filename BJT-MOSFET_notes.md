# 1. BJT-MOSFET_notes

## 1.1. 名词解释
- BJT：bipolar junction transistor
- MOSFET：mental oxide semiconductor field effect transistor
- CMOS：complement-MOS
## 1.2. 参考文档
- [所有笔记的链接](https://gitee.com/AndrewChu/hardware-design) 
- 新概念模拟电路-杨建国

## 1.3. 总结
这里只会介绍工程上使用BJT和MOSFET应该注意的问题。当然了解这两个基础元件的物理模型会帮助理解为什么要这么用。
1. BJT:
	1. BJT的物理结构：电子运动的方向，空间电荷区，内建电场。(学不会就算了)
	2. 关键参数Vce_max Vce_sat Vbe_reverse Ic_max，偏置电流
	3. 直流分析，必须会。
		1. 输出特性曲线：饱和，放大，截止
	2. 交流小信号分析。(学不会就算了)
	3. 输入电阻，输出电阻的计算：
		1. 输出电阻的测量，要把输入信号当做0.
	3. 三种常见的放大电路。共射，共基，射极跟随。背住把，省时省力。
		1. 共射：
			1. 输入电阻：适中
			2. 输出电阻：Rc
			3. 放大倍数：大，反向放大
			4. 速度：因为米勒效应，所以不快
			5. 极性：用于运放的负反馈传递分析。b+  e+  c-
		1. 射极跟随：
			1. 输入电阻：很大
			2. 输出电阻：很小
			3. 放大倍数：1
			4. 速度：适中，因为没有放大倍数，所以没有米勒电容
			5. 极性：b+  e+  c-
		1. 共基：因为速度最快，所以射频常用。带宽很大。(学不会就算了)
			1. 输入电阻：很小
			2. 输出电阻：Rc
			3. 放大倍数：和共射一样大，同向放大。
			4. 速度：快
			5. 极性：e+ b+ c+
	4. 常用的电路：
		1. 差分对：
			1. 共模电流流向：对gnd
			2. 差模电流流向：V+到V-
			3. 就是因为把共模和差模回路分开了，可以分别控制Au，才能有很高的CMRR
			2. 提高CMRR的技巧：使用电流源代替电阻。因为电流源的等效电阻不大，但是动态电阻很大。等效电阻决定了直流工作点，动态电阻决定了AC分析，决定了最终的放大倍数。所以用电流源，可以让CMRR很大。
		2. 镜像电流源：
2. MOSFET:
	1. 物理模型：耗尽层，夹断。
	2. 直流分析：
		1. MOSFET的转移曲线：可变电阻区，恒流区，截止区。（很多很多人都搞不清楚这个）
		2. 转移方程：![Screenshot-2020-08-30 PM1](https://gitee.com/AndrewChu/markdown/raw/master/1598766853_20200830131957992_1155455198.jpg)
	2. 常用的也就是JFET和增强型的MOSFET。耗尽型的MOSFET非常非常少。
	2. JFET的Vgs是负数，Vgs小于0都能导通，到了一个负值就截止。
	3. 增强型MOSFET的Vgs是正数。一定要用Vgs的max参数作为设计指标。
	4. Vgs直接决定了MOS的Id。所以是压控电流器件。
	5. BJT需要偏置电流。MOS不需要偏置电流，只要给Cgs，Cgd充电，所以损耗非常小。
	6. 关键参数：
		1. Qgd
		2. Id
		3. Vds


BJT是模电的第一个难点。   
可能是我比较Low，从来没有用过BJT和MOSFET搭建过放大电路，都是直接用的运放。理由见FAQ里的`为什么大家不用BJT和MOSFET搭建过放大电路`。    
我觉得是因为BJT做放大器的复杂性，导致了大家对于BJT有天然的恐惧，从而使很多很简单，我们工作中会用到的BJT的特性，很多工程师都掌握不好。比如什么是图腾柱，怎么做Rail-to-Rail，更别提很多人搭错了电路。别捡了芝麻，丢了西瓜。这些功能不了解，根本分析不清楚BUS transceiver里的结构。     
学习三极管的知识，一定要有取舍。把很多很复杂，现在也不会用的知识摒弃了，你就会发现BJT也是很简单，很好用的东西。


# 2. BJT
## 2.1. 三极管Transistor

1. 物理模型
   1. 两个PN节，工作在放大区BE结正偏，BC结反偏
   2. 物理接口：
      1. B：基极，控制端
      2. C：集电极，接收电子
      3. E：发射极，电子发射
   3. 工作区
      1. 空间电荷区，耗尽层
      2. 空间电荷区里有**内建电场**。浓度差导致**扩散电流**，电场导致**漂移电流**。最终在空间电荷区达到动态平衡。
      3. PN节正偏，空间电荷区越大

2. 基本特性：
   1. 三个工作区：**饱和，放大，截止**
   2. 电流控制电流
   3. $V_{BE}=0.7V$$  $$I_C=\beta*I_B $
   4. 要让三极管工作在开关状态，那么E极接地，Ib大一点就行，Vce<0.3V
   5. 注意：**如果三极管的E极接了电阻，那么这个三极管一定工作在放大状态**
   6. 三极管的B极最好有上下拉，避免高阻态
   7. **考虑Cbe的寄生电容**，会有米勒效应，带来的时间效应
   8. 要懂得PNP和NPN三极管的用法区别。
      1. NPN的E极接的GND。注意可以接个电容做滤波
      2. PNP的E极接的VCC。注意，不要随意地接个到地的电容做滤波，可以使用接到VCC的电容。
4. 放大电路分析：
	1. 先直流：直流工作点
		1. 理解负反馈电阻Re
			1. 用于稳定直流工作点
			2. 负反馈都会降低放大倍数
		1. 接旁路电容Ce
			1. 可以保证交流通路，没有这个电阻，保证了放大倍数
	1. 交流分析：确定放大倍数
		1. 输入阻抗分析
			1. Re要等效为ß*Re。根据等效原则来
		2. 输出阻抗分析：
			1. 原则1：输入为0
			2. 原则2：施加U，测量I
		3. 增益计算：输出电压/输入电压

## 2.2. 其他特性
想要了解这个，可以参考新概念模拟电路
- 失真
- 频响
- 功率放大器：甲，乙，甲乙类

## 2.3. 获得更大的放大倍数
IC里面的大电阻，都是使用电流源制作的。同时也了解Rc和Re使用电流源替代后，可以不改变直流工作点，但是增大了交流增益。
- Au受到静态参数影响
    - 如果固定Uc，那么Au也固定了
    - 所以把Rc换成电流源，那么静态和动态参数就分开了
        - 静态电阻不大
        - 动态电阻很大
        - 识别电流源  BE两端的电平都定了，那么IC也就定了，就是恒流源

## 2.4. 差动放大电路
理解什么是差模，什么是共模。这个定义只对差分信号有意义
差动放大的回路：
 - 共模的回路：
     - 分析：管子对称，那么两边都是对称的
     - 回路电阻
	     - 单端无负载：Rb+rbe+2(1+ß)Re
 - 差模的回路：
     - 分析：管子对称，那么小信号模型就是从一个管子流到另外一个，不会进过Re
	     - 注意，这个模型是动态模型
	     - Re上的动态电流不变
     - 回路电阻
	     - 单端无负载：2(Rb+rbe)

改进的差动放大电路：
![](https://gitee.com/AndrewChu/markdown/raw/master/1598600783_20200804070222953_1298146548.png)
- 增大共模抑制比：Re变成电流源
     - 动态电阻变大，对共模的抑制变强，但是对差模无影响
 - 增大放大倍数：Rc变成电流源
     - 动态电阻变大，对共模和差模的放大倍数都变大

## 2.5. 三极管搭建的运放
LM324内部框图
- ![](https://gitee.com/AndrewChu/markdown/raw/master/1598600790_20200804073249942_1922677083.png )
## 2.6. 电流扩容
- ![](https://gitee.com/AndrewChu/markdown/raw/master/1598600788_20200804072502914_1209876251.png )




# 3. MOSFET
- ![Screenshot-2020-08-26 PM3](https://gitee.com/AndrewChu/markdown/raw/master/1598429488_20200826160851702_1342324453.jpg)

## 3.1. MOS-FET
### 3.1.1. 缩写
全称：metal oxide semiconductor field effect transistor
cmos： complementary MOS
### 3.1.2. 基本概念
1. 物理模型
	1. 理解反形层，沟道
	2. 理解夹断
	3. 理解三个工作区对应的Vgs和沟道模型
		1. 可变电阻区。Vgs很大，沟道很宽
		2. 恒流区。Vga适中，沟道预夹断，所以可以用Vgs控制沟道的宽度，从而控制Id
		3. 截止区。Vgs很小，没有沟道
2. 转移曲线
	1. 控制信号Vgs和输出信号Id的关系.其实就是输入和输出的关系。
	2. 理解跨导的意义：就是电阻的导数
	3. 电流受控于电压的公式，主要应用于**线性饱和区**。
		1. 可以看到在饱和区放大的时候，Id和Vgs强相关。所以叫线性放大区
		2. Ugsth是曲线开始的点，是开启电压，手册里有。 K可以从数据手册里查到。Ugs是我们的控制电压。
		2. ![Screenshot-2020-08-30 PM1](https://gitee.com/AndrewChu/markdown/raw/master/1598766854_20200830132410534_358019695.jpg)
3. 特性曲线
	1. 特性曲线：Vds和Id之间的关系
	2. ![](https://gitee.com/AndrewChu/markdown/raw/master/1598624600_20200425205935628_893583096.png )
		1. 可变电阻区----开关区域
		2. **饱和区-----用来放大**
		3. 截止区----关断
	1. 理解米勒效应在特性曲线上的工作点转移。
		1. 从截止---恒流---可变电阻
4. 米勒效应：Cgd被放大A倍。结果就是MOS管导通的时候会有一个长时间的米勒平台。要是给MOS的Ig驱动电流不够，那么Cgd充电很慢，MOS导通也会很慢，损耗就会非常大。
	1. ![Screenshot-2020-09-01 PM2](https://gitee.com/AndrewChu/markdown/raw/master/1598948081_20200901143604775_1581230919.jpg)
5. 损耗分析：
	1. Rdson导通损耗：直流电阻带来的损耗
	2. Qg驱动损耗：驱动G极，给IC带来的损耗。
	3. Qgd开关损耗：克服米勒效应，开关时候的损耗。
		1. 两个公式不一样。其实是ΔQ=I*Δt
		2. Qg的损耗，就是IC给的I和V的乘积
		3. 但是Qgd的损耗是负载I和负载V的乘积
	2. ![Screenshot-2020-08-26 PM4](https://gitee.com/AndrewChu/markdown/raw/master/1598431688_20200826163500228_1499949476.jpg)




# 4. FAQ

## 4.1. 为什么大家不用BJT和MOSFET搭建放大电路
这个问题，要了解BJT和MOSFET怎么实现电路的放大。
1. 工程中，我们基本上关注的电压信号。
	1. Vin Vout输入电压被放大成输出电压。所以我们关心的就是这两个信号。
2. BJT是Ib控制的Ic，是电流控制电流。
	1. BJT的Vin是什么？这个很简单，就是我们叠加的AC信号
	2. BJT的Vout？
		1. 想要知道输出电压，那么就要知道Ic，就要知道Ib，这个就是直流工作点分析。
		2. 知道了直流工作点，那就要分析AC信号是怎么样的。很不幸，BJT的AC等效网络的rBE是和Ib有关系的，CE之间也是一个复杂的流控电流源。
		3. 那就要按照AC模型算出AC输入和AC输出的关系。结束
	1. **所以BJT做放大电路是很复杂的。**
		1. 输入输出的关系不是电压控制电压
		2. AC模型和DC模型是分开的。
			1. DC的Vbe是0.7，提供工作点。AC的Vbe是rBE的电压，完全两码事了。
1. MOSFET是Vgs控制Ids。
	1. 貌似比BJT好了一点，但还是还DC分析和AC分析的。所以也很复杂
1. 运放做放大为什么简单。
	1. 原理上，运放的输入信号是个BJT做的差分对，中间是放大电路，输出是个射极跟随，调节输出阻抗。
	2. 因为运放的Aol非常大，导致了运放有了虚短虚断的特性。
	3. 这样，就把运放的很多地方的电压定下来了。直接用KCL或者叠加定义，直接算出了输出电压和输入典雅的关系。
	4. 只要满足运放工作在深度负反馈，就是环路增益大于1的情况。虚短虚断就会成立。
	5. 这个期间，我们不用使用DC和AC分析，就是最基本的电路分析。
		


## 4.2. 搞清楚直流工作点和交流小信号
1. 在放大电路里：第一个提到直流工作点的地方
	1. 直流工作点分析:
		1. 就是放大电路工作的基准点。
		1. 分析方式：把电容都断开
	1. 交流小信号，
		1. 就是叠加在直流工作点的有用交流小信号，最终被放大。直流工作点没有选好，就会导致放大后的交流信号被截断。
		2. 分析方式：把电容都短路，把恒定电平的点标为GND，把电流源短路。
			1. 为什么？因为叠加定义
1. 在频域分析：第二个会提到直流工作点的地方
	1. 真对运放的幅频特性分析。需要在输入叠加一个AC小信号，看电路的输出。然后不断地改变f，就可以画出波特图。
	2. 这个时候，要保证运放工作在稳定的工作点，不然就是没有意义的。
	3. 注入的AC信号，要保证不能影响到直流工作点。
		1. AC信号太小：输出信号被噪声淹没
		2. AC信号太大：输出信号畸变了，因为工作点变了。
	4. 其实和放大电路的分析一样

## 4.3. 什么是图腾柱，怎么实现Rail-to-Rail
- 图腾柱：
	- NPN在上面，PNP在下面。公用B。
		- 就是两个射极跟随。输入输出有个0.7V的压降。产生了交越失真。当然也可以在前端加二极管减少交越失真
		- 安全，每次肯定就一个管子导通
- Rail-to-Rail:
	- 输出的Rail-to-Rail：
		- PNP在上面，NPN在下面，B分开
			- B不分开会怎么样：两个管子都导通了，电路直通了。
			- 优点是，输入输出的压降是Vcesat，可以做到30mV。这个已经满足了Rail-to-Rail。
	- 输入的Rail-to-Rail：
		- 差分对可以是2个NPN的，也可以是2个PNP的。
		- 如果是2个NPN的，那么输入可以到Rail，但是不能到0，只能到0.7
		- 如果是2个PNP的，那么输入不可以到Rail，可以到0。
		- 所以输入的Rail-to-Rail，要做2对管子。可以实现Rail-to-Rail。
		- 问题：两对管子的偏置电流不一样，Ib跳变导致输入交越失真，THD变大
		- 第二种方式：
			- 只用NPN的管子，内置电荷泵，把VCC抬上去，那就可以输入到rail了。







## 4.4. 三极管型号互换会怎么样，CE互换会怎么样
1. 总有功力不到家的Engineer用错三极管：  
	1. case1：很多人用惯了NPN，在高边开关该用PNP的，他们也用了NPN。那么到底能不能互换呢？ 
	2. case2: 用对了三极管，但是CE互换了。    


1. 回答：
	1. case1：
		1. 互换了，貌似影响不大，会变成一个射极跟随器，只不过是从开关状态变成了放大状态。
		2. 但是关键是，违背了设计的初衷。不要那么做。
	2. case2：
		1. 三极管工作的时候，要发射结正片，集电结反偏。
		2. 互换了CE，那么三极管工作时候的两个PN结都是正常工作的
		3. 但是，不能忽视CE的差别
		1. C集电结：掺杂浓度不高，很宽，电子接收很容易。
		2. E发射结：掺杂浓度很高，比较窄，所以电子发射很容易。
		2. CE互换，这个时候三极管就会变成放大倍数很低的器件。该发射的E，结果在接收。
		3. 电路也能用。但是这个也不是我们设计的样子。




## 4.5. 不要再把开关电路错误的搭成放大电路
1. 危害
	1. 工程上，还是经常使用三极管作为开关的，一旦错误的使用成了放大电路。会导致信号的裕度变得很差，三极管吃了很大的电平。
	2. 最可怕的是，测试的时候是正常的。温度一遍，Vce就会变，刚好卡在阈值上的电压就会失效。
	3. 还是好好分析电路，别将就。



## 4.6. 为什么都用NMOS
1. 根本原因：同样的die(IC的面积)，做出来的PMOS的参数，会比NMOS差非常多。
	1. 同样的Id，PMOS非常大，NMOS可以做的很小。die的大小又决定了结电容，结电容大了，被米勒效应一倍增，那就更大了，开关损耗就非常大。
1. NMOS的优点
	2. 价格低
	2. 参数好
		1. Id
		2. Vds
	1. 速度快
		1. 结电容小
	1. 缺点是，需要自举电容，给高边的nmos做驱动
		1. Vb  Vs接电容两边
		2. Vb接二极管后，接VCC
		3. 当上管关闭，Vcc给电容充电
		4. 当上管开启，电容往外释放能量
1. 这个就是为什么很多的H桥，都是用了4个一样的NMOS，宁可加电荷泵。

































