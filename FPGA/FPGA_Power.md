# 1. FPGA_Power

## 1.1. 参考资料
- [FPGA的电源](http://xilinx.eetrend.com/d6-xilinx/article/2018-05/12852.html)
## 1.2. 名词解释

## 1.3. 总结
1. 电压：
	1. 有精度要求
	2. 有上电时序的要求
	3. 只能单调上升，不能下降
1. 电流：
	1. 电流很大
	2. 会瞬间抽大电流。那么就要加很多的电容，但是电容加多了电源会不稳定
	3. 瞬间的大电流，可能就要考虑走线的电感，就需要仿真电源的环路稳定性了
	4. 选了很多电容。但是电容会有ESR，所以要选陶瓷电容。但是电压上升了，电容的容量会下降。这个也要考虑 


