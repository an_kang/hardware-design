# 1. Books_Summary

## 1.1. 乔布斯传
还没看
## 1.2. 坏血
强烈推荐工程师和项目管理者看。   
[坏血](https://gitee.com/AndrewChu/hardware-design/blob/master/Books/Bad%20Blood%20.md)

## 1.3. atomic habits
每个人，想要养成好习惯，都可以看看。     
[Atomic Habits](https://gitee.com/AndrewChu/hardware-design/raw/master/PDF/Atomic-Habits.pdf)

## 1.4. 巴拉巴西成功学
[巴拉巴西成功学](https://gitee.com/AndrewChu/hardware-design/raw/master/PDF/巴拉巴西成功定律.pdf)

## 1.5. 墨菲定律
[墨菲定律](https://gitee.com/AndrewChu/hardware-design/raw/master/PDF/墨菲定律.pdf)
## 1.6. 哈佛大学凌晨四点半
[哈佛大学凌晨4点半](https://gitee.com/AndrewChu/hardware-design/blob/master/Books/哈佛凌晨4点半.md)